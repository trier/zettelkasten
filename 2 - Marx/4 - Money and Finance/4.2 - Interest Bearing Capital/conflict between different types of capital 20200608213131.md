# Conflict between different types of capital
tags: #finance_capital


Commercial capital, Interest Bearing Capital, Rentiers and Industrial Capital must always be vying for a bigger slice of the pie - just as none of them realise that all profit depends on labour, so finance and commercial capitalists don't realise that all their profit depends on production - because to them capital is a commodity they alienate. See [[20200608204953]] fetishisation of IBC


Can we see 20th century as oscillation in this sense? From productive capital to financial capital, now potentially back to productive?? Or potentially to rentiers??

Finance capital got a big slice of the pie from 80s onward - perhaps depressing productive activity? The evidence seems to bear that out??

>It is in fact only the division of capitalists into money capitalists and industrial capitalists that transforms a part of the profit into interest and creates the category of interest at all ; and it is only the competition between these two kinds of capitalist that creates the rate of interest.
- KIII 493