# Debt monetisation
#debt-monetisation #banking #interest_bearing_capital

==need to come back to this==


> (Note for later elaboration.) A particular form of credit. We know that when money functions as means of payment instead of means of purchase, the commodity is alienated first and its value realized only later. If payment takes place only after the commodity has been re-sold, this sale does not appear as a consequence of the purchase, but rather it is by the sale that the purchase is realized. Sale, in other words, ,becomes a means of purchase. - Secondly,. certificates of debt, bills, etc. become means of payment for the creditor. - Thirdly, money is replaced by the settlement of out'­ standing debt certificates.