# Price of money
tags: #interest-rate #division-of-profit

**The price of a commodity is derived from the labour used to make it - interest is derived from the surplus labour in other commodities**

>If interest is spoken of as the price of money capital, this is an irrational form of price, in complete contradiction with the concept of the price of a commodity.59 Here, price is reduced to its purely abstract form, completely lacking in content, as simplyparticular sum of money that is paid for something which some­ how or other figures as a use-value ; whereas in its concept, price is the value of this use-value expressed in money.
- KIII 47