# Hoarding represents breakdown of circulation
#hoarding #debrunhoff

"Hoarding is an interruption in the circulation of commodities; the series of exchanges is broken and temporarily confined to the exchange C-M. This break reflects a desire to fasten M down and keep it. "The money becomes petrified into a hoard, and the seller becomes a hoarder of money." Hoarding is a demand for money as money, the general equivalent possessing special qualities that distinguish it from all commodities."
- de Brunhoff, Marx on Money, p. 39

"Gold and silver "remain liquid as the crystallisation of the process of circulation. But gold and silver establish themselves as money only in so far as they do not function as means of circulation."
- de Brunhoff, Marx on Money, p. 39.

*It is only by removing money from circulation that money can be validated as continuing to function as a measure of value, thereby in turn enabling it to continue as the medium of circulation*