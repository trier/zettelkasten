# Hoarding is congealing of money as money
#hoarding #marx

> "The difference of nature and quantity between the measure of value and the currency both have the effect of separating the general equivalent from its money form, the specific commodity which functions as such in practice. From this arises hoarding, the third function of money, which gold fulfills when "in person **or by representative**, it congeals into the sole form of value, the only adequate form of existence of exchange-value, in opposition to use-value, represented by all other commodities."
- de Brunhoff, Marx on Money, p. 38.