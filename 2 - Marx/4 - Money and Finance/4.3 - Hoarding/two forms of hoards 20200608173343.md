# Two forms of hoards
tags: #hoarding



1. Reserve fund for purchase and payment
2. Capital waiting to be invested

> Firstly, the accumulation of money as a hoard, in this case as the section of capital that must always exist in the money form, as a reserve fund of means of purchase and payment. This is the first form of the hoard, as it reappears in the capitalist mode of pro­ duction and generally comes into being with the development of commercial capital, at least for the use of this capital. In both cases this applies as much to international circulation as to domestic. This hoard is in constant flu x, constantly spilling out into circulation and returning from it. The second form of the hoard is that of idle capital temporarily unoccupied in the money form, together with newly accumulated money capital that has not been , invested. The functions that this hoard formation itself makes necessary start with its storage, book-keeping, etc.
- KIII 435