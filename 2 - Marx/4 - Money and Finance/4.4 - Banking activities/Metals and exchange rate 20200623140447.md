# Metals and exchange rate
tags: #commodity-money #gold #banking #central_banking

**International trade can lead to a long term increase/decrease in national gold reserves**

```
(5) The function of the metal reserve held by a so-called national . bank, a function that is far from being the only thing governing the size of the metal reserve, since this can grow simply through the crippling of domestic and foreign business, is threefold: (i) a reserve fund for international payments, i.e. a reserve fund of world money; (ii) a reserve fund for the alternately expandinga.nd contracting domestic metal circulatio n ; (iii) (and this is connected with the banking function and has nothing to do with the function of money as simple money) a reserve fund for the payment of deposits and the convertibility of notes. It can therefore also be affec�ed by conditions that bear on only one of these three functions. 
```
- KIII 701-2

