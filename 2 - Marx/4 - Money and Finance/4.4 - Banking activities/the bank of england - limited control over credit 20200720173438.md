# The bank of england - limited control over credit
#credit-creation #central-banking

**Private credit creation was a major force, even under these conditions**

> Suppose the Bank were to allow the discount rate to remain at 5% at a time the money Market was in an easy state, and when everyone was when therefore discounting Bank, the emulating at would 2'/:%. discount The escompteurs, all its business instead under of its very nose. Nowhere is this shown more clearly than in the history of the Bank of England after the 1844 Act,” which made the Bank a real of the rival private Bankers in the discount business, etc. The Bank of England, in order to secure itself a share, and a growing share, of the discount business during the periods of easiness in the money market, was continually forced to lower its discount rate, not only to the level maintained by the private Bankers, but often below it. Its “regulation of credit” is therefore to be taken cum grano salis,” whereas Darimon makes his superstitious belief in the Bank’s absolute control of the money market and of credit the starting point of his argument.
[@marxMarxEngelsCollected2010, 63]

