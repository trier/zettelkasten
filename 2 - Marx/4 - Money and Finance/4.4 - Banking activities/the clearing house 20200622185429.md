# The clearing house
tags:  #clearing-house #settlement #payments #banking

> Pure economizing on means of
circulation appears in its most highly developed form in the clear­
ing house, the simple exchange of bills falling due, where the
principal function of money as means of payment is simply to
settle the balances. But the existence of these bills of exchange
itself depends on the credit that the industrialists and merchants
give one another. If this credit declines, so does the number of
bills, particularly the long-term ones, and so too therefore does the
effectiveness of this method of settlement. And this economy that
consists in the removal of money from transactions and depends
entirely on the function of money as means of payment, depending
in turn on credit, must be one of two kinds (apart from the greater
or lesser development of technique in the concentration of these
payments) : reciprocal claims, represented by bills of exchange or
cheques, are balanced by the same banker, who simply transfers
the claim from one person's account to the other's ; or else the various bankers settle them among them selves. 11 The concentra­ tion of £8-£10 million in bills of exchange in the hands of a bill­ broker, such as the firm of Overend, Gurney & Co.; was one of the principal means for expanding the scope of this settlement at the local level.
- KIII 654

> But if B deposits the money received in pay­ ment from A with his banker, who passes it to C in discounting a bill of exchange, C buying from D, D depositing it with his banker and the latter lending it to E, who buys from F, then even its velocity as a mere means of circulation (means of purchase) is mediated by several credit operations : B's depositing with his banker and the latter's discounting for C, D's depositing with his banker and the latter's discounting for E ; four credit operations in all. Without these credit operations, the same piece of money would not have performed five purchases successively in a given period of time. The fact that it changed hands .  without t�e mediation of actual purchase and sale - as a deposIt and by dispositive decline between 1 844 and 1 857, even though trade more than doubled, as is shown by the export and import figures.
- KIII 656

> And this went parallel with the increase in gold circulation which was so marked at this very time. Notes of higher denominations (£200 to £1 ,000), on the other hand, declined from £5,856,000 in 1 852 to £3,241 ,000 in 1 857, a decline of more than £2t million. This has 'been ex­ plained as follows : ' On the 8th June 1 854, the private bankers of London admitted the joint-stock banks to the arrangements of the clearing house, and shortly afterwards the final clearing was adjusted in the Bank of England. The daily clearances are now effected by transfers in the accounts which the several banks keep in that establishment. In consequence of the adoption of this system, the large notes which the bankers formerly employed for the purpose of adjusting theitr accounts are no longer necessary.
- KIII 656
== Clearinghouses allow faster circulation - less money is required to actually facilitate these transactions==
