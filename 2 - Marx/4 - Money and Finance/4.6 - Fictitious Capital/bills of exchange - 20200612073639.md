# Bills of exchange
tags: #bills-of-exchange

**Bills of exchange transform commodity-capital into credit money-capital, which can then be used as means of purchase, or transformed into banknotes/gold, which can then be used as means of payment**

> Drawing bills of exchange is transforming commodities into a form of credit money, just as discounting bills is transforming this credit money into a different money, i.e. banknotes. Overstone at least concedes that the purpose of discounting is to receive money. Previously he had claimed that discounting was not to transform capital from one form into the other, but simply to obtain additional capital.
- KIII 557

> '3743. What is the great desire of the mercantile community under pressure of panic, such as you state to have occurred in 1825, 1837 and 1839; is their object to get possession of capital or the legal tender? - Their object is to get the command of capital to support their business.'

**Whether money obtained through bank operations is money-capital or means of payment depends on the type of operation - is the bank customer obtaining a loan purely on the basis of expectations about future surplus value, or are they selling something to the bank? **

>Their object is to obtain means of payment for  bills on themselves that fall due, on account of the shortage of credit that has set in, and not to have to unload their commodities below their proper price. If they do not have any capital at all themselves, of course they obtain capital with these means of payment, since they obtain value without an equivalent. The demand for money as such always consists simply in the desire to convert value from the form of commodiites or creditor's claims into the form of money. Hence, even aside from crises, the great distinction between borrowing capital and discounting, the latter being simply the transformation of monetary claims from one form into another, or into actual money itself.
- KIII 557

> In Chapter 32 Marx says essentially the same thing : ' The ".demand for means of payment is simply a demand for convertibility into money, in so far as the merchants and producers are able to offer good security ; it is a demand for money capital, in so far as this is not the case, i.e. in so far as· an advance of means of payment gives them not only the money form, but also the f!quivalent that they lack for payments, in whatever form this might be ' [po 648]. And in Chapter 33 : ' When the credit system is Jleveloped, so that money is concentrated in the hands of the banks, jt iscthey who advance it, at least nominally. This . advance is only telated .to the nwney in circulati()n. It is an advance of circulation" llot .an advance of the capitals it circulates ' [p. 664, :Engf!ls's ,emphasis]. Mr Chapman, too, who. ought to know, confirms. the 'above interpretation of the discount business (B. A . .  1857) :  'The banker has the bill, the banker has bought the bill' (Evidence Question 5 1 39) [Engels's emphasis].
- KIII 559 (Engels)