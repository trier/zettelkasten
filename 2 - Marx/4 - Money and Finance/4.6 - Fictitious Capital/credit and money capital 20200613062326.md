# Confusion re banking capital
tags: #marx #fictitious_capital #finance #interest_bearing_capital #money

_Credit becomes nothing more than a set of claims on labour_

> In as much as we have so far considered the specific form of accumulation of money capital, and of money wealth in general, this reduces itself to the accumulation of proprietary claims to labour. Accumulation of capital in the form of the national debt, as we have shown, means nothing more than the growth of a class of state creditors with a preferential claim to certain sums ,from the overall proceeds of taxation. 6 In the way that even an accumu·lation of debts can appear as an accumulation of capital, we see the distortion involved in the credit system reach its culmination. These promissory notes which were issued for a capital originally borrowed but long since spent, these paper duplicates of annihil­ ated capital, function for their owners as capital in so far as they are saleable commodities and can therefore be transformed back into capital.
- KIII
-