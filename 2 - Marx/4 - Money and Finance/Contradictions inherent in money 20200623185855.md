# The contradictions of the money form
#contradiction #money #marx #contradictions-of-money

> All contradictions of the money system and of the exchange of products under the money system lie in the development of the relationship of products as exchange values, of their role as exchange value or simply as value.
[@marxMarxEngelsCollected2010, 89]

The contradictions begin with use value and exchange value. Exchange value only exists if the commodity is a use-value, but the exchange value is only expressed when it is separated from the commodity, articulated in the face of a completely different use value.

This contradiction gives rise to the development of the general equivalent.

The capitalist class has been caught between its desire for ever-increasing liquidity
and its need to have confidence that its assets are stable and durable. The capitalist
state and financial sector have historically tried to broker a compromise between these
needs with convertible currency regimes and their intertwined fractional reserve
credit systems.

This compromise has always been terminally unstable, and the financial history of
capitalism is in part a story of the ongoing attempt to create institutional
arrangements that resolve these contradictions. They have instead simply displaced
the contradictions, which re-emerge in new institutional arrangements.
The most recent of these displacements occurred when the United States abandoned
gold convertibility in 1971, and in doing so created one of the conditions for what we
now describe as financialisation – an immense global liquidity pool somehow
seemingly unaffected by questions of confidence.