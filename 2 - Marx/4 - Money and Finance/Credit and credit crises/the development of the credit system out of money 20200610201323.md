#  development of credit system out of money

**This links to the material on merchants and money-bearing capital, articulating the way in which capitalist takes pre-capitalist monetary phenomena (hoarding, simple lending, etc) which develop out of the universal equivalent, and generalise them into a system to support capitalism itself.**

[[20200608183609]] money dealing capital oldest form of capital 20200608183609.md
[[20200609103247]] IBC as pre-capitalist form 20200609103247.md
[[20200608140427]] combination of comm capital and credit 20200608140427.md
[[20200608155122]] historical development of comm capital 20200608155122.md
[[20200608184814]] 5A - Pre-capitalist forms summary20200608184814.md
[[20200607175958]] Money accumulated as usury becomes capital20200607175958.md
[[20200607182927]] Gradual evolution of monetary trade 20200607182927.md


**The credit system is accelerated with the formation of joint stock companies [[20200612090242]] socialisation of capitalist production (joint stock companies)**