# The hoarding drive is boundless in nature
#hoarding #marx

> The hoarding drive is boundless in its nature. Qualitatively or formally considered, money is independent of all limits, that is it is the universal representative of material wealth because it is directly convertible into any other commodity. But at the same time every actual sum of money is limited in amount, and therefore has only a limited efficacy as a means of purchase. This contradiction between the quantitative limitation and the qualitative lack of limitation of money keeps driving the hoarder back to his Sisyphean task: accumulation. He is in the same situatiuon as a world conqueror, who discovers a new boundary with each country he annexes.
[@marxCapitalCritiquePolitical1990, 230-1]

