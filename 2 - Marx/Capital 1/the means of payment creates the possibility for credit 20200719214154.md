# Title: Credit emerges from the function of money as means of payment
#development-of-credit #credit #means-of-payment

> Credit-money springs directly out of the function of money as a means of payment, in that certificates of debts owing for already purchased commodities themselves circulate for the purpose of transferring those debts to others. On the other hand, the function of money as a means of payment undergoes expansion in propor­ tion asthe system of credit itself expands. As the means of payment money takes on its own peculiar forms of existence, in which it inhabits the sphere of large-scale commercial transactions. Gold and silver coin, on the other hand, are m ostly relegated to the sphere of retail trade. 5 4
[@marxCapitalCritiquePolitical1990, 228]

> The development o f money a s a means o f payment makes it ne c�SS<!I"Y to accumulate it in preparation for the days when the sums which are owing fall due. While hoarding, considered as an independent form of self-enrichment, vanishes with the advance of bourgeois society [die biirgerliche Gesellschaft], it grows at the same time in the form of the accumulation of a reserve fund of the means of payment.
[@marxCapitalCritiquePolitical1990, 239-40]