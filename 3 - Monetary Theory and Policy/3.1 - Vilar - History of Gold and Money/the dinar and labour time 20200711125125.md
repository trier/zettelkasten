# Dinar and labour time
tags: #commodity-money #vilar

In the West the dinar was often called the ‘mancus’: the origin of the word is unknown,  but it seems to come from the Arabic manqusha (literally ‘engraved’), a piece of money coined or engraved with fine lettering. The fineness of the Muslim inscriptions seems to have reinforced the prestige of the coin. 
[@vilarHistoryGoldMoney1976, 31]

**Interesting - this implies that these coins were more valuable precisely because more labour time had been used in their production**