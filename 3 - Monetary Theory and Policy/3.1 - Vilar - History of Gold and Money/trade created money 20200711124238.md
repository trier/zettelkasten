# Money emerges out of trade
#world_money #vilar #commodity-money #history-of-money

> To sum up, we can say that money proper appeared late in the day, and it did so on the periphery of the trading system of the ancient world and not within the great empires. Trade created money rather than money trade.
@vilarHistoryGoldMoney1976, 31

