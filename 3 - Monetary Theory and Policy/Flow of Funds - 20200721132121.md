# Flow of Funds
#flow_of_funds #money #finance


Sources and uses

Goods and services - expenditure and receipts
Financial assets
Accumulation/decumulation
Repayment/borrowing
Dishoarding/hoarding


Every use has a corresponding source and vice versa
Every agent's use is another agent's source, and vice versa