# Task 1

News consumption/climate change caused by human activity/natural processes

Left/right affiliation OR income and need to get respect from other people

Influence over policy decisions at work/general life satisfaction

Influence over policy:
Coding:

Old:

0 = Extremely dissatisfied
1 = 1
2 = 2
etc..
10 = Extremely satisfied
77 = Refusal
88 = Don't know
99 = No answer

New:

0, 1 = Extremely dissatisfied
2, 3 4 = Dissatisfied
5 = Neither satisfied nor dissatisfied
6, 7, 8 = Satisfied
9, 10 = Extremely satisfied
77 = Refusal
88 = Don't know
99 = No answer


Life satisfaction:

0 = I have/had no influence
10 = I have/had complete control
66 = NA
77= Refusal
88 = Don't know
99 = No answer

New:

0, 1 = Extremely low influence
2, 3, 4 = Low influence
5 = Moderate influence
6, 7, 8 = High influence
9, 10 = Extremely high influence


1. 2 nominal or ordinal variables
2. Bivariate analysis
3. Report using table
4. Cross-tabulation
5. Test association between two variables
6. Interpret overall results


**Income quintiles**

0-17,290
17,291 - 25,660
25,661 - 35,500
35,5001-49,780
49,781 or more0-0

1 = Less than once a month
2 = Once a month
3 = Several times a month
4 = Once a week
5 = Several times a week
77 = Refusal
88 = Don't know
99 = No answer


# Task 2
1. Two numerical variables



# Task 3
List and define measures of central tendency
2. Hypothesis testing question




$H_0: \mu = 47$
$H_a: \mu\ !=  47$
$\alpha = 0.05$
$z = \frac{\bar{x}-\mu}{s \div \sqrt{1500}}$ 
$z = \frac{41.12-47}{16.3 \div \sqrt{1500}}$ 
$z = -13.97123$

$p = 0.49$
$\hat{p} = 0.52$
$n = 1500$ 

$H_0: \mu = \bar{X}$ 

$H_a: \mu\ !=  \bar{X}$

$\alpha = 0.05$

$\bar x = 41.12$
$\mu = 47$
$s = 16.3$
$n = 1500$

$t = \frac{\bar{x}-\mu}{s \div  \sqrt{1500}}$ 

$t = \frac{41.12-47}{16.3 \div \sqrt{1500}}$ 

$t = \frac{-5.88}{0.4209}$

$t = -13.9701$

$df = 1499$

$p = .00001$





41.12 - 47 = -5.88
16.3 / sqrt 1500

sqrt 1500 = 38.729833462


$H_0: p = \hat{p}$ 


$H_a: p != \hat{p}$ 

$p = 0.52$ 

$$
z = {p-\Pi} \div {\sqrt{\frac{\Pi(1-\Pi)}{n}}}
$$ 

$z = 0.52 - 0.49 \div \sqrt\frac{0.49(1-0.49)}{n}$ 

$z = 0.03 \div \sqrt\frac{0.2499}{1500}$
$z = 0.03 \div \sqrt{0.0001666}$ 
$z = 0.03 \div 0.012907362$ 
$z = 232.42549052$


$H_0:$