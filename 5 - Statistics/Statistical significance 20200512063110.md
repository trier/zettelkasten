# Statistical significance

Statistical significance represents the level mof Type I error we are willing to live with.
It is the probability we are prepared to risk of rejecting $H_0$ when it is in fact true.

The statistical power of a test refers to its ability to detect an effect.
Power of at least .8 is good.
Power is affected by sample size and significance level $\alpha$ set.


# Steps

1. Set up hypotheses
2. Obtain the sample statistic on which to base your test - i.e. sample mean $\bar{x}$ = 80.
3. Calculate the test statistic.
![Screenshot from 2020-05-12 06-35-03.png](/home/user/Zettlekasten - Zettlr/Statistics/Screenshot from 2020-05-12 06-35-03.png)

5. Make a decision

## Central limit theory

If $H_0$ is true, then z-score should look like a value from the standard normal distribution.
== Need to look into this more ==

![Screenshot from 2020-05-12 06-49-14.png](/home/user/Zettlekasten - Zettlr/Statistics/Screenshot from 2020-05-12 06-49-14.png)
We know the z-score should fall within the relevant range in the normal distribution.


## Confidence interval method

* The 95% confidence interval for the mean

$\bar{x} +/- 1.96\frac{s}{\sqrt{n}}$

## Direction of hypotheses

### Non-directional hypothesis
There will be a difference between sample and population, but no view as to what direction - both tails of the distribution will be critical regions. We would be performing a 'two tailed' test.

### Directional hypothesis
We have a view about the nature of the difference - the critical region will be at one end of the distribution. this is a one tail test.
