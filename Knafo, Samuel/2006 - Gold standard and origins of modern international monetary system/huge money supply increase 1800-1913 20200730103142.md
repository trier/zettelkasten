> the emphasis on the restrictive nature of the gold standard seems at odd with the dramatic expansion of the supply of money in the 19th century. Between 1800 and 1913, the stock of money (M1) in England and Wales jumped from £50 to £1264.2 million; that is a 2528% increase (Cameron, 1967, p. 42).
[@knafoGoldStandardOrigins2006, 84]

> Eichengreen, for ex- ample, argues that the lack of demand for social and expansionary policies was a stabilizing feature of the gold standard. However, to characterize the gold standard in this way neglects the fact that, at that time, monetary policy had never been conducted with this expansionary aim. The kind of expansionist policies that are deemed incompatible with the gold standard had never existed before the gold standard. 5

Expansionary monetary policy wasn't actually conducted before the gold standard…

> Metallic coins constituted the central component of pre-modern monetary circulation. This metallic basis provided little elasticity for the supply of money since the increase of the stock of money required the addition of new gold or new silver. This meant that without mines producing these metals most European states were obliged to attract precious metal from outside in order to increase their supply of money (de Roover, 1949). The inelasticity of the money supply was a central preoccupation of the period and it largely explains the mercantilist emphasis on the stock of precious metal (Wordie, 1997). With the monetization of social exchanges acceler- ating during this period, demands for means of payment were frequently heard
[@knafoGoldStandardOrigins2006, 86]

Fixed money supply under metallic circulation.

> For this reason, the problem of liquidity within a country could not be disentangled from the practices around currency exchange.
[@knafoGoldStandardOrigins2006, 87]\

Manipulation of exchange under metallic system - 
* Manipulation of par exchange rate internally - nominal value v metallic value
* Manipulation of international exchange rates

Then the UK comes up with the gold standard - enables them to simultaneously _increase money supply_ while maintaining the value of the pound. (Knafo 2006, 88)

This was enabled by banknotes...

> England pursued what appears, from our vantage point, as some sort of sound monetary policy. Its striking feature was its ability to increase its stock of money while still maintaining with relative ease the value of the sterling pound from the mid 17th century until 1914. This trajectory was intimately linked to the rise of modern banking in England and the profound transformation of its monetary system that accompanied it. Its distinctive feature was the elaboration by banks of an efficient and relatively stable strategy for creating fiduciary money, mostly in the form of banknotes initially (Quinn, 1994). This allowed them to play an increasing role in monetary circulation. The success of this method was clearly reflected in the fact that banknotes came to represent over 60% of the monetary circulation by the early 19th century (Marchal and Picquet- Marchal, 1977: 84).
[@knafoGoldStandardOrigins2006, 88]


> The rising credibility of banknotes led to an extension in their use, as the practices of bankers became increasingly important to the overall condition of the economy (Hoppit, 1986) However, as competitive pressures on banks increased, banks began to take greater risks leading to rapidly increasing issues of banknotes and new inflationary pressures (Pressnel, 1956).
[@knafoGoldStandardOrigins2006, 88]

_Including monetisation_

Importance of agriculture - demmands for liquidity - seasonally inluenced