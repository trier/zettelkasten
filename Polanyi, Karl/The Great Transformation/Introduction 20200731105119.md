Anyone looking into this topic is going to have to contend with two fundamental questions and the literature that surrounds them, and want to know how Marx fits into them.
What is money? There are three main schools of thought on this topic:
1. 'Metallism' - money is a commodity.
2. The credit theory of money - money is a promise to pay. All money is credit - it represents a title to a portion of social wealth.
3. Chartalism - money is a product of state fiat. States create money through their taxing and spending activity. The state creates money by requiring taxes to be paid using a particular object

Within these frameworks, there is then a question about the role of banks.

1. Financial intermediares. 
2. Fractional reserve lenders
3. Credit creators

So, where does Marx fit into this debate? The common wisdom is that Marx outlines a metallist theory of money, in which banks play the role of financial intermediaries.

I'm going to outline a different interpretation here. I argue that Marx outlines a theory in which money is simultaneously a commodity, credit, and a product of state fiat, and banks play the role of financial intermediaries, fractional reserve lenders and credit creators.

Chartalism always in conflict with metallism in early period.