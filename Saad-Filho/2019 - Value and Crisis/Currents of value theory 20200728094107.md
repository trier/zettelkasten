# Currents of value theory
#value

## Traditional Marxism

1. The concepts introduced in ch 1-3 of capital are concepts that apply to non-capitalist modes of production
2. Concept of value is necessary for determining rate of exploitation
3. Analysis of profit requires determination of commodity prices.
4. Determination of relative prices is done by assuming capitals have equal value compositions.

## Sraffian
