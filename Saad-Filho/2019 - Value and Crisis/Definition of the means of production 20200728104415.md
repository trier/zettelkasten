# Definition of the means of production
#means-of-production

1. Generalised commodity production (production for exchange) for profit
2. Workers and means of production separated and reunited through wage-relation

> 3. Capitalism is a mode of production, social reproduction and exploitation with three essential features: the diffusion of commodity production; the sepa- ration between the workers and the means of production (monopolised by the capitalist class), the commodification of labour power and the generalisation of the wage relation; and the subordination of production by the profit motive. These features, and their relations of mutual implication, mean that capitalism is a totality: it exists only at the level of society. It is meaningless to speak of capitalism at the ‘individual’ level (e.g. in a small number of farms or factories submerged in a sea of non-capitalist social relations) or of ‘wage relations’ be- tween isolated employers and temporary workers producing small quantities for largely closed communities, in which most needs are not satisfied by com- modity exchanges.
[@saad-filhoValueCrisisEssays2019, 48-9]