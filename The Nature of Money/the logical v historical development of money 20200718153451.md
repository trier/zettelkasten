# The logical v historical development of money
#history-of-money

**This has echoes of Marx's introduction to Grundrisse??**

> Here again there is exhibited a tendency among certain economists to compare what appears in today’s conditions to be the logical order with the actual complex chronological development of money over its long and convoluted history. The logical listing of functions in the table therefore implies no priority in either time or importance, for those which may be both first and foremost reflect only their particular time and place.
[@daviesHistoryMoneyAncient2002, 28]

